## Campus Network Boot - W.I.P

**Note:** If you use this system, please give me some feedback! I'd like to know! Feel free to open issues and the such. 


### IPXE
IPXE is a network boot firmware. See [here](http://ipxe.org/).

We make use of IPXE as a bootable image for either USB, DHCP netboot, or CD boot media. 

### Network Install with Preconfig
With the above, and the ability to install over the network, we can install full distros on any PC on the Uni network and configure it for use with-in the Uni network.

For example, in the Ubuntu Distros we preconfigure the apt repoes and the network time settings such that a user can get setup more quickly. 

## How to use
In short, boot to one of the images provided, whilst on the Uni network (or off if you are using the USB boot!), and chose an option from the menu that appears. 

The boot image is very small, and simply connects you to this projects pages site. You can view all the config files and artifacts here: https://sulug.pages.cs.sun.ac.za/netboot 

These images are setup to configure some basic campus details. See the preseeds for details. 

### USB 
The USB option is probably the easiest. 

**General**
1. Download the `usb_efi.img` from the [list](#ipxe_images) below.
1. Write this to a flash drive using either `dd` (if you know what you are doing) or a program like [etcher](https://www.balena.io/etcher/) (my personal favourite and usually available in the software repository)
    1. It's a small file, so it will be fast! 
1. Boot from the USB stick and you will be presented with the campus Netboot screen. Have fun!

**Older machines and special hardware**
* If you want to boot from  BIOS/legacy mode, please use the plain `usb.img`.
* If you have a USB network adapter, you might need to use the `add_drivers` varient. This isn't the default because it seems to not work on other machines that normally do work. 

### DHCP server
On the CS network, simply enable and select netboot from your PC/laptop while being connected to the LAN (wifi won't work).

### CD ROM for VM and ... cd readers
If you wish to install in a VM, try the `disk.iso` as your installation disk. 

## IPXE images

Both the tftp server image and USB images reside below.

Quick Links:
* [disk.iso](https://sulug.pages.cs.sun.ac.za/netboot/disk.iso)
* [undionly.kpxe](https://sulug.pages.cs.sun.ac.za/netboot/undionly.kpxe)
* [usb.img](https://sulug.pages.cs.sun.ac.za/netboot/usb.img)
* [usb_add_drivers.img](https://sulug.pages.cs.sun.ac.za/netboot/usb_add_drivers.img)
* [usb_add_drivers_efi.img](https://sulug.pages.cs.sun.ac.za/netboot/usb_add_drivers_efi.img)
* [usb_efi.img](https://sulug.pages.cs.sun.ac.za/netboot/usb_efi.img)

## References
Ref for the iPXE picture: https://ipxe.org/
