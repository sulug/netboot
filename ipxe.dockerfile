# Ref https://github.com/skrysmanski/docker-ipxe-builder/blob/master/base-image/Dockerfile
FROM reg.cs.sun.ac.za/computer-science/docker-prebuild/alpine-tools:latest
RUN apk update && apk add --update cdrkit syslinux bash gcc git make perl musl-dev xz-dev coreutils openssl \
 && rm -rf /var/cache/apk/*

# Check out iPXE
# See: http://ipxe.org/download and https://git.cs.sun.ac.za/sulug/ipxe-mirror
RUN git clone https://git.cs.sun.ac.za/sulug/ipxe-mirror.git /work_dir

# Add HTTPS support
RUN sed -i -e 's/#undef[\t \s]*DOWNLOAD_PROTO_HTTPS.*$/#define DOWNLOAD_PROTO_HTTPS/g' \
/work_dir/src/config/general.h

RUN sed -i -e 's/.*#define[\t \s]*PARAM_CMD.*$/#define PARAM_CMD/g' \
/work_dir/src/config/general.h

WORKDIR /work_dir/src

# Pre-build a few things
RUN make bin/undionly.kpxe bin/ipxe.usb bin/ipxe.iso bin-x86_64-efi/ipxe.efi bin-x86_64-efi/ipxe.usb
