FROM library/node:alpine

ARG AJV_CLI_VERSION=3.2.1
RUN npm install -g ajv-cli@${AJV_CLI_VERSION}

ADD validator-data/* /data/

WORKDIR "/data"
